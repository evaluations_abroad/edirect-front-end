import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import promise from "redux-promise"; //allow async actions
import multi from "redux-multi"; //allow async actions
import thunk from "redux-thunk"; //allow async actions

import rootReducer from "./state/reducers";
import { setupInterceptors } from "./utils/util";
import "./index.css";

//import * as serviceWorker from './serviceWorker';
import Routes from "./main/routes";

const devTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

const store = applyMiddleware(multi, thunk, promise)(createStore)(
  rootReducer,
  devTools
);

setupInterceptors(store);

ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
