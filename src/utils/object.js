export const pick = (object, ...pickedKeys) => {
  const pickedEntries = Object.entries(object).filter(([key]) => pickedKeys.includes(key));
  return Object.fromEntries(pickedEntries);
};

export const dissect = (object, ...rejectedKeys) => {
  const pickedEntries = Object.entries(object).filter(([key]) => !(rejectedKeys.includes(key)));
  return Object.fromEntries(pickedEntries);
};
