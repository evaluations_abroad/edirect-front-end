import { showMessage } from "../state/app/appActions";

const messages = {
  generic: {
    success: { message: "Successful operation.", status: "success" },
    error: { message: "Operation error.", status: "error" },
    service_unavailable: { message: "Service unavailable!", status: "error" },
    not_authorized: {
      message: "Você não possui permissão para acessar esse recurso!",
      status: "error",
    },
    not_authenticated: {
      message: "Not authenticated, please login!",
      status: "error",
    },
  },
  user: {
    update_success: {
      message: "Seus dados foram atualizados",
      status: "success",
    },
  },
  password: {
    update_success: {
      message: "Senha atualizada com sucesso",
      status: "success",
    },
  },
  copy_to_clipboard: {
    success: {
      message: "A mensagem foi copiada para a área de transferência",
      status: "success",
    },
  },
  token: {
    validation_failure: {
      message: "Não foi possível validar esse token",
      status: "error",
    },
  },
};

export default (action, messagesNode = "generic") => {
  const messageNode = messages[messagesNode];
  if (!messageNode) {
    console.error(`${messagesNode} is not a valid message node.`);
    return;
  }
  const messageAction = messageNode[action];
  if (!messageAction) {
    console.error(
      `${action} is not a valid message type from ${messagesNode} node.`
    );
    return;
  }
  return showMessage(messageAction.message, messageAction.status);
};
