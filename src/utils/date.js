export const transformDateRangeToQueryParameters = (queryObjectPropertyName, dateRangeStart, dateRangeEnd) => ({
  [queryObjectPropertyName + '_gte']: dateRangeStart,
  [queryObjectPropertyName + '_lte']: dateRangeEnd
});

export const calculateDaysDiff = (date1Str, date2Str) => {
  if (!date1Str || !date2Str) {
    return;
  }
  const MS_PER_DAY = 1000 * 60 * 60 * 24;

  const [date1DateObject, date2DateObject] = [date1Str, date2Str].map(dateStr => new Date(dateStr));
  const [date1UTC, date2UTC] = [date1DateObject, date2DateObject].map(dateObject =>
    Date.UTC(dateObject.getFullYear(), dateObject.getMonth(), dateObject.getDate())
  );

  return Math.floor((date2UTC - date1UTC) / MS_PER_DAY);
};

export const calculateFromDate = (dateStr, dateOperations = {}) => {
  const dateObject = new Date(dateStr);
  if (Number.isNaN(dateObject.getTime())) {
    console.error(`${dateStr} is not in a valid date format!`);
    return;
  }

  const validOperations = ['add', 'subtract', 'set'];
  const dateOperationsMap = {
    'year': { get: 'getFullYear', set: 'setFullYear' },
    'month': { get: 'getMonth', set: 'setMonth' },
    'day': { get: 'getDate', set: 'setDate' },
    'hours': { get: 'getHours', set: 'setHours' },
    'minutes': { get: 'getMinutes', set: 'setMinutes' },
    'seconds': { get: 'getSeconds', set: 'setSeconds' },
    'milliseconds': { get: 'getMilliseconds', set: 'setMilliseconds' },
  };

  return Object.entries(dateOperations).reduce((accumulator, [operationTarget, operationObject]) => {
    if (!(operationTarget in dateOperationsMap)) {
      console.error(`${operationTarget} is not a valid date operation target.`);
      return accumulator;
    }
	  return Object.entries(operationObject).reduce((operationAccumulator, [operation, operationValue]) => {
		  if (!(validOperations.includes(operation))) {
        console.error(`${operation} is not a valid operation!`);
        return operationAccumulator;
      }
		  switch(operation) {
        case 'add': return new Date(
          new Date(operationAccumulator)[dateOperationsMap[operationTarget]['set']](
            operationAccumulator[dateOperationsMap[operationTarget]['get']]() + operationValue
            )
          );
        case 'subtract': return new Date(
          new Date(operationAccumulator)[dateOperationsMap[operationTarget]['set']](
            operationAccumulator[dateOperationsMap[operationTarget]['get']]() - operationValue
            )
          );
        case 'set': return new Date(
          new Date(operationAccumulator)[dateOperationsMap[operationTarget]['set']](operationValue)
        );
        default: return operationAccumulator;
      }
    }, accumulator);
  }, dateObject);
};
