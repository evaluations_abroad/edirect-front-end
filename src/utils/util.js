import axios from "axios";
import pigeon from "./pigeon";
import { doLogout } from "../state/auth/authActions";

//whereFilters template: {prop:{eq:'value'}} or {prop:'value'}
export function gerarObjectJsonFiltros(limit, offset, whereFilters, filters) {
  var filter = {};
  filter.limit = limit;
  filter.offset = offset;
  if (filters) {
    Object.keys(filters).forEach((key) => {
      filter[key] = filters[key];
    });
  }

  if (whereFilters) {
    var where = {};
    Object.keys(whereFilters).forEach((key) => {
      if (whereFilters[key].eq) {
        where[key] = { eq: whereFilters[key].eq };
      } else if (whereFilters[key].like) {
        where[key] = { like: "%" + whereFilters[key].like + "%" };
      } else {
        where[key] = { like: "%" + whereFilters[key] + "%" };
      }
    });
    filter.where = where;
  }
  return filter;
}

export function gerarJsonFiltros(limit, offset, whereFilters, filters) {
  return JSON.stringify(
    gerarObjectJsonFiltros(limit, offset, whereFilters, filters)
  );
}

export function gerarCountFiltros(whereFilters) {
  var where = "";
  if (whereFilters && Object.keys(whereFilters).length > 0) {
    where += "?";
    Object.keys(whereFilters).forEach((key) => {
      let value = whereFilters[key].value || whereFilters[key];
      let operador = "[like]";
      let percent = "%";

      if (whereFilters[key].matchMode === "eq") {
        operador = "";
        percent = "";
      }

      where +=
        "where[" + key + "]" + operador + "=" + percent + value + percent + "&";
    });
  }
  return where;
}

export function gerarJsonCount(whereFilters) {
  return whereFilters ? JSON.stringify(gerarCountFiltros(whereFilters)) : "";
}

export function setupInterceptors(store) {
  axios.interceptors.response.use(
    function (response) {
      return response;
    },
    function (error) {
      if (error.message === "Network Error") {
        store.dispatch(pigeon("service_unavailable"));
      }
      if (error.response) {
        if (error.response && error.response.status === 403) {
          store.dispatch(pigeon("not_authorized"));
        } else if (error.response && error.response.status === 401) {
          store.dispatch([
            doLogout(),
            //pigeon('not_authenticated')
          ]);
        } else {
          store.dispatch(pigeon("error"));
        }
      }
      return Promise.reject(error);
    }
  );
}

export function convertDateToHourAndMinute(date) {
  const hour = `${date.getHours()}`.padStart(2, "0");
  const minute = `${date.getMinutes()}`.padStart(2, "0");
  return `${hour}:${minute} `;
}

export function convertDateToString(date) {
  const day = `${date.getDate()}`.padStart(2, "0");
  const month = `${date.getMonth() + 1}`.padStart(2, "0");
  const year = date.getFullYear();
  return `${month}/${day}/${year}`;
}

export function convertDateHourToString(date) {
  const formatDate = convertDateToString(date);
  const hour = convertDateToHourAndMinute(date);
  return `${formatDate} ${hour}`;
}

const DECIMAL_BRL_SEPARATOR_CLOAKER = ",";

export function convertBRLStringToNumber(sNumber) {
  return Number.parseFloat(
    sNumber.toString().replace(DECIMAL_BRL_SEPARATOR_CLOAKER, ".")
  );
}
