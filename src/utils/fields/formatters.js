export const date = (dateStr, format = 'pt-BR', timeZone = 'America/Sao_Paulo') => {
  const dateObject = new Date(dateStr);
  if (Number.isNaN(dateObject.getTime())) {
    console.error(`${dateStr} is not in a valid date format!`);
    return;
  }
  return dateObject.toLocaleDateString(format, { timeZone });
};

export const cpf = (cpfStr) => {
  const cleanCPFStr = cpfStr.replace(/\D/g, '');
  const CPF_CHAR_LENGTH = 11;

  if (cleanCPFStr.length !== CPF_CHAR_LENGTH) {
    console.error(`Provided CPF (${cleanCPFStr}) must be ${CPF_CHAR_LENGTH} characters long!`);
    return;
  }

  const CPFRegExp = /^(\d{3})(\d{3})(\d{3})(\d{2})$/;
  const CPFRegExpExec = CPFRegExp.exec(cleanCPFStr).slice(1);
  const CPFDigits = CPFRegExpExec.slice(0, 3);
  const identifierDigits = CPFRegExpExec.slice(3, 4);
  return [CPFDigits.join('.'), identifierDigits].join('-');
};

export const monetary = (monetaryFloat, currencySymbol, thousandsSeparator, decimalsSeparator = '.', decimalPlaces = 0) => {
  const monetaryValue = Number.parseFloat(monetaryFloat).toFixed(decimalPlaces).replace('.', decimalsSeparator);
  if (Number.isNaN(monetaryValue)) {
    console.error(`Could not cast ${monetaryFloat} to float`);
    return;
  }
  let monetaryStr = monetaryValue.toString();

  if (thousandsSeparator) {
    monetaryStr = monetaryStr.replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSeparator);
  }
  if (currencySymbol) {
    monetaryStr = `${currencySymbol} ${monetaryStr}`;
  }
  return monetaryStr;
};

export const percentage = (percentageFloat, decimalsSeparator = '.', decimalPlaces = 0) => {
  const percentageValue = Number.parseFloat(percentageFloat).toFixed(decimalPlaces).replace('.', decimalsSeparator);
  if (Number.isNaN(percentageValue)) {
    console.error(`Could not cast ${percentageFloat} to float`);
    return;
  }



  return `${percentageValue}%`;
};