export default {
  common: {
    effectiveDate: 'Início de vigência',
    expiryDate: 'Fim de vigência',
    ge: 'Garantia estendida',
    rf: 'Roubo ou furto'
  }
};
