const messages = {
  required: "Campo requerido",
  no_match_passwords: "Password must match",
  malformed_mail: "Email is not valir",
  password_minimum_chars: "Password must have at minimun 8 chars",
  name_minimum_chars: "Password must have at minimun 3 chars",
  invalid_format: "Invalid format",
  signup_success_title: "Account created! :)",
  signup_title: "Create you account",
  signin_title: "Login",
  signup_button_label: "Send",
  signin_button_label: "Enter",
  recover_password_button_label: "Send",
  back_button_label: "Back",
};
export default messages;
