import React, { Component } from "react";
import { connect } from "react-redux";
import { compose, bindActionCreators } from "redux";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Tooltip from "@material-ui/core/Tooltip";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

import message from "../../../utils/messages";
import Yup from "../../../utils/yup-customs";
import FormDialog from "../../../commons/FormDialog";
import {
  get,
  listProjects,
  remove,
  create,
  update,
  updateTask,
  createTask,
  removeTask,
  listTasks,
} from "../ducks/actions";
import ProjectCard from "../components/ProjectCard";

const styles = (theme) => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: "100vh",
    overflow: "auto",
  },
  tableContainer: {
    height: 320,
    padding: "10px 0px",
  },
  headerContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
  },
  fabAdd: {
    position: "absolute",
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
  iconButton: {
    padding: "5px",
  },
  cardSearchForm: {
    margin: "10px 0 10px 0",
  },
  buttonMargin: {
    margin: theme.spacing.unit * 2,
  },
});

class ProjectList extends Component {
  state = {
    openDialogProject: false,
    openDialogTasks: false,
    projectNameToEdit: "",
    projectIdToEdit: null,
  };

  create = () => {
    this.setState({ openDialogProject: true });
  };

  edit = (selected) => {
    this.setState({
      projectNameToEdit: selected.name,
      projectIdToEdit: selected.id,
      openDialogProject: true,
    });
  };

  remove = (selected) => {
    const { remove, listProjects } = this.props;
    confirmAlert({
      title: "Remove",
      message: "Confirm deletion?",
      buttons: [
        {
          label: "OK",
          onClick: () => remove(selected).then(() => listProjects()),
        },
        {
          label: "Cancel",
        },
      ],
    });
  };

  removeTask = async (selected) => {
    const { removeTask, listProjects } = this.props;
    await removeTask(selected);
    listProjects();
  };

  componentWillMount() {
    this.props.listProjects();
  }

  loadGridData = () => {
    this.props.listProjects();
  };

  handleCloseProjectDialog = () => {
    this.setState({
      openDialogProject: !this.state.openDialogProject,
      projectIdToEdit: null,
      projectNameToEdit: "",
    });
  };

  handleClickSaveProject = async (values) => {
    const { create, update, listProjects, userId } = this.props;
    if (this.state.projectIdToEdit) {
      await update({ id: this.state.projectIdToEdit, ...values, userId });
    } else {
      await create({ ...values, userId });
    }
    this.setState({
      openDialogProject: !this.state.openDialogProject,
      projectIdToEdit: null,
      projectNameToEdit: "",
    });
    listProjects();
  };

  showTasks = async ({ id: projectId }) => {
    await this.props.listTasks(projectId);
    this.setState({
      projectIdToEdit: projectId,
      openDialogTasks: true,
    });
  };

  handleClickSaveTask = async (values) => {
    const { createTask, updateTask, listProjects } = this.props;
    if (values.id) {
      await updateTask(values);
    } else {
      await createTask(values);
    }
    listProjects();
  };

  renderProjectForm = () => {
    const schema = Yup.object().shape({
      name: Yup.string().required(message.required),
    });

    return (
      <FormDialog
        open={this.state.openDialogProject}
        handleClose={() => this.handleCloseProjectDialog()}
        title="Create new project"
        description=""
        initialValues={{
          name: this.state.projectNameToEdit,
        }}
        fields={[
          {
            variant: "outlined",
            name: "name",
            label: "Project name",
            autoComplete: "off",
            type: "text",
          },
        ]}
        validationSchema={schema}
        buttons={[
          {
            label: "Save",
            type: "submit",
            fullWidth: true,
            variant: "contained",
            color: "secondary",
          },
          {
            label: "Cancel",
            type: "button",
            fullWidth: true,
            variant: "contained",
            color: "default",
            onClick: () => this.handleCloseProjectDialog(),
          },
        ]}
        handleSubmit={this.handleClickSaveProject}
      />
    );
  };

  render() {
    const { classes, projects } = this.props;
    const { openDialogProject } = this.state;

    return (
      <div>
        <div className={classes.appBarSpacer} />
        <div className={classes.tableContainer}>
          <Card>
            <CardContent>
              <Grid container>
                {projects.length === 0 ? (
                  <Grid item xs={12}>
                    <Typography variant="h6" component="h2">
                      No projects on your board, create one to start
                    </Typography>
                  </Grid>
                ) : (
                  projects.map((project) => (
                    <ProjectCard
                      key={project.id}
                      project={project}
                      onDeleteProject={() => this.remove(project)}
                      onEditProject={() => this.edit(project)}
                      onSaveTask={(values) => this.handleClickSaveTask(values)}
                      onDeleteTask={(selected) => this.removeTask(selected)}
                      onEditTask={(values) => this.handleClickSaveTask(values)}
                    />
                  ))
                )}
              </Grid>
            </CardContent>
            <Tooltip title="Create new project" aria-label="">
              <Fab
                className={classes.fabAdd}
                size="medium"
                color="secondary"
                onClick={this.create}
              >
                <AddIcon />
              </Fab>
            </Tooltip>
          </Card>
        </div>
        {openDialogProject ? this.renderProjectForm() : ""}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  projects: state.project.projects,
  tasks: state.project.tasks,
  loading: state.project.loading,
  userId: state.auth.user.id,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      get,
      listProjects,
      remove,
      create,
      update,
      createTask,
      updateTask,
      removeTask,
      listTasks,
    },
    dispatch
  );
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(ProjectList);
