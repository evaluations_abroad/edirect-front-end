import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";

import { convertDateHourToString } from "../../../utils/util";

const styles = (theme) => ({
  tooltip: {
    backgroundColor: "#f5f5f9",
    color: "rgba(0, 0, 0, 0.87)",
    border: "1px solid #dadde9",
  },
});

const DoneTask = (props) => {
  const { classes } = props;
  return (
    <Tooltip
      data-testid="DoneTask"
      className={classes.tooltip}
      title={
        <>
          <Typography color="inherit">
            <p>
              Task created at{" "}
              {convertDateHourToString(new Date(props.task.createdAt))}
            </p>
            <p>
              Task finished in{" "}
              {convertDateHourToString(new Date(props.task.finishDate))}
            </p>
          </Typography>
        </>
      }
    >
      <p data-testid="TaskDescription">
        <Checkbox
          disabled
          checked={true}
          inputProps={{ "aria-label": "primary checkbox" }}
        />
        {props.task.description}
      </p>
    </Tooltip>
  );
};

export default withStyles(styles)(DoneTask);
