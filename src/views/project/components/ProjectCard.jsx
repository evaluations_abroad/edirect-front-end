import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardHeader from "@material-ui/core/CardHeader";
import IconButton from "@material-ui/core/IconButton";
import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Tooltip from "@material-ui/core/Tooltip";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import Checkbox from "@material-ui/core/Checkbox";

import Yup from "../../../utils/yup-customs";
import FormDialog from "../../../commons/FormDialog";
import message from "../../../utils/messages";
import DoneTask from "./DoneTask";

const styles = (theme) => ({
  projectCard: {
    padding: "10px",
  },
  fabAdd: {
    bottom: "10px",
    left: "80%",
  },
  taskToDo: {
    backgroundColor: "#f5f5f9",
    color: "rgba(0, 0, 0, 0.87)",
    border: "1px solid #dadde9",
  },
  emptyProject: {
    color: "#58A4C1",
    fontWeight: "bold",
    textAlign: "center",
  },
});

class ProjectCard extends Component {
  state = {
    openDialogFormTask: false,
    taskDescriptionToEdit: "",
    taskIdToEdit: null,
  };

  handleOpenFormTask = () => {
    this.setState({ openDialogFormTask: true });
  };

  handleCloseFormTask = () => {
    this.setState({
      openDialogFormTask: false,
      taskDescriptionToEdit: "",
      taskIdToEdit: null,
    });
  };

  handleClickSaveTask = (values) => {
    const {
      project: { id: projectId },
    } = this.props;
    this.props.onSaveTask({
      ...values,
      id: this.state.taskIdToEdit,
      projectId,
    });
    this.handleCloseFormTask();
  };

  renderTaskForm = () => {
    const schema = Yup.object().shape({
      description: Yup.string().required(message.required),
    });

    return (
      <FormDialog
        open={this.state.openDialogFormTask}
        handleClose={() => this.handleCloseFormTask()}
        title="Create new task"
        description=""
        initialValues={{
          description: this.state.taskDescriptionToEdit,
        }}
        fields={[
          {
            variant: "outlined",
            name: "description",
            label: "Task description",
            autoComplete: "off",
            type: "text",
          },
        ]}
        validationSchema={schema}
        buttons={[
          {
            label: "Save",
            type: "submit",
            fullWidth: true,
            variant: "contained",
            color: "secondary",
          },
          {
            label: "Cancel",
            type: "button",
            fullWidth: true,
            variant: "contained",
            color: "default",
            onClick: () => this.handleCloseFormTask(),
          },
        ]}
        handleSubmit={this.handleClickSaveTask}
      />
    );
  };

  handleClickRemoveTask = (id) => {
    confirmAlert({
      title: "Remove",
      message: "Confirm deletion?",
      buttons: [
        {
          label: "OK",
          onClick: () => {
            this.props.onDeleteTask({ id });
            this.handleCloseFormTask();
          },
        },
        {
          label: "Cancel",
        },
      ],
    });
  };

  handleClickTaskDone = (selected) => {
    confirmAlert({
      title: "Mark as done",
      message: "Confirm task done?",
      buttons: [
        {
          label: "OK",
          onClick: () => {
            this.props.onSaveTask({
              ...selected,
              finishDate: new Date(),
            });
          },
        },
        {
          label: "Cancel",
        },
      ],
    });
  };

  editTask = (selected) => {
    this.setState({
      taskDescriptionToEdit: selected.description,
      taskIdToEdit: selected.id,
      openDialogFormTask: true,
    });
  };

  render() {
    const { classes, project } = this.props;

    return (
      <Grid item lg={3} sm={4} xs={6} className={classes.projectCard}>
        <Card variant="outlined">
          <CardHeader
            action={[
              <IconButton
                color="default"
                className={classes.iconButton}
                onClick={this.props.onEditProject}
              >
                <Edit fontSize="small" />
              </IconButton>,
              <IconButton
                color="default"
                className={classes.iconButton}
                onClick={this.props.onDeleteProject}
              >
                <Delete fontSize="small" />
              </IconButton>,
            ]}
            title={project.name}
          />
          <CardContent>
            <Typography variant="h6" component="h2">
              To do tasks
            </Typography>
            {project.tasks.length > 0 ? (
              project.tasks
                .filter((t) => !t.finishDate)
                .map((task) => (
                  <Grid container className={classes.taskToDo}>
                    <Grid item xs={8}>
                      <Checkbox
                        checked={false}
                        onChange={() => this.handleClickTaskDone(task)}
                        inputProps={{ "aria-label": "primary checkbox" }}
                      />
                      {task.description}
                    </Grid>
                    <Grid item xs={4}>
                      <Tooltip title="Edit task" placement="bottom">
                        <IconButton
                          color="default"
                          className={classes.iconButton}
                          onClick={() => this.editTask(task)}
                        >
                          <Edit fontSize="small" />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Remove task" placement="bottom">
                        <IconButton
                          color="default"
                          className={classes.iconButton}
                          onClick={() => this.handleClickRemoveTask(task.id)}
                        >
                          <Delete fontSize="small" />
                        </IconButton>
                      </Tooltip>
                    </Grid>
                  </Grid>
                ))
            ) : (
              <div className={classes.emptyProject}>No tasks to do</div>
            )}
            <Typography variant="h6" component="h2">
              Tasks done
            </Typography>
            {project.tasks.length > 0 ? (
              project.tasks
                .filter((t) => t.finishDate)
                .map((task) => <DoneTask task={task} />)
            ) : (
              <div className={classes.emptyProject}>No tasks done</div>
            )}
          </CardContent>
          <CardActions>
            <Tooltip title="Create new task" aria-label="">
              <Fab
                className={classes.fabAdd}
                size="small"
                color="secondary"
                onClick={this.handleOpenFormTask}
              >
                <AddIcon />
              </Fab>
            </Tooltip>
          </CardActions>
        </Card>
        {this.renderTaskForm()}
      </Grid>
    );
  }
}

export default withStyles(styles)(ProjectCard);
