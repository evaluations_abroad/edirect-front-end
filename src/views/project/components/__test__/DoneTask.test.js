import React from "react";
import ReactDOM from "react-dom";
import { render, cleanup } from "@testing-library/react";
//import "@testing-library/jest-dom/extend-expect";

import renderer from "react-test-renderer";

import DoneTask from "../DoneTask";

// afterEach(cleanup);

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <DoneTask task={{ createdAt: "", finishDate: "", description: "" }} />,
    div
  );
});

it("renders DoneTask correctly", () => {
  const description = "Task test";
  const { getByTestId } = render(
    <DoneTask task={{ createdAt: "01", finishDate: "", description }} />
  );
  expect(getByTestId("TaskDescription")).toHaveTextContent(description);
});

it("renders DoneTask correctly", () => {
  const description = "Task test 2";
  const { getByTestId } = render(
    <DoneTask task={{ createdAt: "01", finishDate: "", description }} />
  );
  expect(getByTestId("TaskDescription")).toHaveTextContent(description);
});

it("matches snapshot", () => {
  const description = "Task test snapshot";
  const tree = renderer
    .create(<DoneTask task={{ createdAt: "", finishDate: "", description }} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("matches snapshot 1", () => {
  const description = "Task test snapshot 2";
  const tree = renderer
    .create(<DoneTask task={{ createdAt: "", finishDate: "", description }} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
