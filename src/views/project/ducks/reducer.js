import { INITIAL_VALUES } from "./actions";
import types from "./types";

export default (state = INITIAL_VALUES, action) => {
  switch (action.type) {
    case types.PROJECT_LIST_FETCH_STARTED:
      return { ...state, loading: true };
    case types.PROJECT_LIST_FETCH_FINISHED:
      return { ...state, projects: action.payload, loading: false };
    case types.PROJECT_CREATED:
      return { ...state };
    case types.PROJECT_FETCHED:
      return {
        ...state,
        project: action.payload,
      };
    case types.PROJECT_UPDATED:
      return { ...state };
    case types.TASK_LIST_FETCH_STARTED:
      return { ...state, loadingTasks: true };
    case types.TASK_LIST_FETCH_FINISHED:
      return { ...state, tasks: action.payload, loadingTasks: false };
    case types.TASK_CREATED:
      return { ...state };
    case types.TASK_FETCHED:
      return {
        ...state,
        task: action.payload,
      };
    case types.TASK_UPDATED:
      return { ...state };
    default:
      return state;
  }
};
