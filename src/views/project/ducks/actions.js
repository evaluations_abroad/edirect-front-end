import ApiAgent, { HTTP_METHOD } from "../../../main/apiAgent";
import resources from "../../../main/resources";
import types from "./types";
import pigeon from "../../../utils/pigeon";

export const INITIAL_VALUES = {
  projects: [],
  tasks: [],
  project: {
    name: "",
  },
  disabledForm: false,
  loading: false,
  loadingTasks: false,
};

export const listProjects = async () => (dispatch) => {
  dispatch({ type: types.PROJECT_LIST_FETCH_STARTED });
  ApiAgent.submit(
    `${resources.PROJECTS_RESOURCE}/userProjects`,
    HTTP_METHOD.GET,
    {
      params: {},
    }
  ).then((response) => {
    dispatch({
      type: types.PROJECT_LIST_FETCH_FINISHED,
      payload: response.data,
    });
  });
};

export const listTasks = async (projectId) => (dispatch) => {
  dispatch({ type: types.TASK_LIST_FETCH_STARTED });
  return dispatch(
    ApiAgent.getList(
      resources.TASKS_RESOURCE,
      {},
      {
        projectId,
      },
      types.TASK_LIST_FETCH_FINISHED
    )
  );
};

export const get = async (id, dispatchType) => (dispatch) => {
  ApiAgent.submit(resources.PROJECTS_RESOURCE, HTTP_METHOD.GET, {
    params: { id },
  }).then((response) => {
    dispatch({
      type: types.PROJECT_FETCHED,
      payload: response.data,
    });
    if (dispatchType) {
      dispatch({ type: dispatchType });
    }
  });
};

export const create = async (values) => (dispatch) => {
  ApiAgent.submit(resources.PROJECTS_RESOURCE, HTTP_METHOD.POST, {
    params: values,
  }).then((response) => {
    if (response && response.data.id) {
      dispatch([{ type: types.PROJECT_CREATED }, pigeon("success")]);
    } else {
      dispatch(pigeon("error"));
    }
  });
};

export const createTask = async (values) => (dispatch) => {
  ApiAgent.submit(resources.TASKS_RESOURCE, HTTP_METHOD.POST, {
    params: values,
  }).then((response) => {
    if (response && response.data.id) {
      dispatch([{ type: types.TASK_CREATED }, pigeon("success")]);
    } else {
      dispatch(pigeon("error"));
    }
  });
};

export const update = async (values) => (dispatch) => {
  ApiAgent.submit(resources.PROJECTS_RESOURCE, HTTP_METHOD.PUT, {
    params: values,
  }).then((response) => {
    if (response && response.data.id) {
      dispatch([{ type: types.PROJECT_UPDATED }, pigeon("success")]);
    } else {
      dispatch(pigeon("error"));
    }
  });
};

export const updateTask = async (values) => (dispatch) => {
  ApiAgent.submit(resources.TASKS_RESOURCE, HTTP_METHOD.PUT, {
    params: values,
  }).then((response) => {
    if (response && response.data.id) {
      dispatch([{ type: types.TASK_UPDATED }, pigeon("success")]);
    } else {
      dispatch(pigeon("error"));
    }
  });
};

export const remove = async (values) => (dispatch) => {
  ApiAgent.submit(resources.PROJECTS_RESOURCE, HTTP_METHOD.DELETE, {
    params: values,
  }).then(() => {
    dispatch([{ type: types.PROJECT_DELETED }, pigeon("success")]);
  });
};

export const removeTask = async (values) => (dispatch) => {
  ApiAgent.submit(resources.TASKS_RESOURCE, HTTP_METHOD.DELETE, {
    params: values,
  }).then(() => {
    dispatch([{ type: types.TASK_DELETED }, pigeon("success")]);
  });
};
