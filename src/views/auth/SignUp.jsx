import React, { Component, Fragment } from "react";
import { compose, bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import { doSignUp, doSignIn } from "../../state/auth/authActions";
import message from "../../utils/messages";
import Yup from "../../utils/yup-customs";
import CustomInput from "../../commons/fields/CustomInput";
import FormComponent from "../../commons/FormComponent";
import resources from "../../main/resources";

const styles = (theme) => ({
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  message: {
    color: "rgb(34, 144, 62)",
    padding: 30,
  },
});

class SignUp extends Component {
  state = {
    message: "",
    email: "",
    showForm: true,
  };

  handleSubmit = async (values) => {
    const { doSignIn, doSignUp, history } = this.props;
    delete values.passwordConfirm;
    const resp = await doSignUp(values);
    if (resp.data.id) {
      setTimeout(() => {
        doSignIn({ email: values.email, password: values.password }).then(
          () => {
            history.push("/");
          }
        );
      }, 5000);
    }
  };

  sendToSingIn = () => {
    this.props.history.push(`/${resources.SIGNIN_RESOURCE}`);
  };

  render() {
    const { classes, loading } = this.props;
    const { showForm } = this.state;
    return (
      <Fragment>
        {loading ? <CircularProgress /> : ""}
        <Typography component="h1" variant="h5" color="primary">
          {showForm ? message.signup_title : message.signup_success_title}
        </Typography>
        <div className={classes.form}>
          {showForm ? this.renderForm() : this.renderMessage()}
          <Button
            fullWidth
            variant="outlined"
            color="default"
            className={classes.submit}
            onClick={this.sendToSingIn}
          >
            {message.back_button_label}
          </Button>
        </div>
      </Fragment>
    );
  }

  renderMessage() {
    const { classes } = this.props;
    const { message } = this.state;
    return (
      <Typography
        className={classes.message}
        align="center"
        variant="subheading"
      >
        {message}
      </Typography>
    );
  }

  renderForm() {
    const schema = Yup.object().shape({
      email: Yup.string()
        .email(message.malformed_mail)
        .required(message.required),
      password: Yup.string()
        .required(message.required)
        .min(8, message.password_minimum_chars),
      passwordConfirm: Yup.string()
        .equalTo(Yup.ref("password"), message.no_match_passwords)
        .required(message.required),
    });

    return (
      <FormComponent
        handleSubmit={this.handleSubmit}
        validationSchema={schema}
        initialValues={{
          email: "",
          password: "",
          passwordConfirm: "",
        }}
        enableReinitialize={true}
        buttons={[
          {
            label: message.signup_button_label,
            type: "submit",
            fullWidth: true,
            variant: "contained",
            color: "secondary",
          },
        ]}
      >
        <CustomInput name="email" label="Your email" sm={12} maxLength={40} />
        <CustomInput
          name="password"
          label="Your password"
          sm={12}
          maxLength={8}
          type="password"
        />
        <CustomInput
          name="passwordConfirm"
          label="Confirm your password"
          sm={12}
          maxLength={8}
          type="password"
        />
      </FormComponent>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: state.auth.loading,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ doSignUp, doSignIn }, dispatch);
export default compose(
  withRouter,
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(SignUp);
