import React, { Component, Fragment } from "react";
import { compose, bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";

import resources from "../../main/resources";
import message from "../../utils/messages";
import { doSignIn } from "../../state/auth/authActions";
import FormWithFormik from "../../commons/FormWithFormik";
import Yup from "../../utils/yup-customs";

const styles = (theme) => ({
  form: {
    width: "100%",
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  link: {
    textDecoration: "none",
    paddingTop: 12,
  },
});

class SignIn extends Component {
  handleSubmit = async (values) => {
    const user = await this.props.doSignIn(values);
    if (user) {
      this.props.history.push("/");
    }
  };

  sendToSignUp = () => {
    this.props.history.push(`${resources.SIGNUP_RESOURCE}`);
  };

  render() {
    const { classes, loading } = this.props;

    const schema = Yup.object().shape({
      email: Yup.string().email().required(),
      password: Yup.string().required(),
    });

    return (
      <Fragment>
        {loading ? <CircularProgress /> : ""}
        <Typography component="h1" variant="h5" color="primary">
          Login
        </Typography>
        <div className={classes.form}>
          <FormWithFormik
            initialValues={{
              email: (this.props.location.state || { email: "" }).email,
              password: "",
            }}
            fields={[
              {
                name: "email",
                label: "Email",
                autoComplete: "off",
                variant: "outlined",
                margin: "dense",
              },
              {
                name: "password",
                label: "Password",
                autoComplete: "off",
                type: "password",
                variant: "outlined",
                margin: "dense",
              },
            ]}
            validationSchema={schema}
            buttons={[
              {
                label: "Submit",
                type: "submit",
                fullWidth: true,
                variant: "contained",
                color: "secondary",
              },
            ]}
            handleSubmit={this.handleSubmit}
          />
          <Button
            data-testid="login-button"
            fullWidth
            variant="outlined"
            color="default"
            className={classes.submit}
            onClick={this.sendToSignUp}
          >
            {message.signup_title}
          </Button>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: state.auth.loading,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ doSignIn }, dispatch);
export default compose(
  withRouter,
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(SignIn);
