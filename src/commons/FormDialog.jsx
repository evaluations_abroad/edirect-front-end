import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormWithFormik from './FormWithFormik';

export default class FormDialog extends Component {

  state = {
    open: this.props.open || false
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ open: nextProps.open });
  }

  handleClose = () => {
    this.setState({ open: this.props.handleClose ? this.props.handleClose() : false });
  };

  render() {

    const { open } = this.state;

    const {
      title,
      description,
      initialValues,
      fields,
      validationSchema = {},
      buttons = [],
      formStyle = {},
      handleSubmit = () => alert('Implementar o handleSubmit')
    } = this.props;

    return (
      <div>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">{title}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {description}
            </DialogContentText>
            <FormWithFormik
              initialValues={initialValues}
              fields={fields}
              validationSchema={validationSchema}
              buttons={buttons}
              handleSubmit={handleSubmit}
              formStyle={formStyle}
            />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

FormDialog.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  description: PropTypes.string,
  initialValues: PropTypes.object.isRequired,
  fields: PropTypes.array.isRequired,
  validationSchema: PropTypes.object,
  buttons: PropTypes.array.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};