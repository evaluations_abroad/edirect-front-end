import React from 'react';
import { Field, ErrorMessage, connect } from 'formik';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass, inputClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass,
  root: inputClass.root,
  item: inputClass.gridItemFormField
});

class CustomInput extends React.Component {
  handleChange = (e) => {
    // eslint-disable-next-line react/prop-types
    const { formik, onChange, name } = this.props;
    // eslint-disable-next-line react/prop-types
    formik.setFieldValue(name, e.target.value);
    if (onChange) {
      onChange(e);
    }
  }

  CustomInputComponent = ({ field, ...props }) => {
    const { disabled, maxLength, label, type = 'text', margin = 'dense' } = props;
    return (
      <TextField
        {...field} disabled={disabled} autoComplete='off'
        label={label} inputProps={{ maxLength: maxLength }}
        variant='outlined' type={type} fullWidth
        margin={margin} onChange={this.handleChange}
      />
    );
  };

  render() {
    const { classes, name, label, xs = 12, sm = 6, style = {}, type, disabled = false } = this.props;
    return (
      <Grid item xs={xs} sm={sm} style={style} className={classes.item}>
        <Field
          name={name}
          type={type}
          label={label}
          disabled={disabled}
          component={this.CustomInputComponent}
        />
        <ErrorMessage
          name={name} className={classes.errorMessage}
          component={CustomErrorMessageComponent}
        />
      </Grid>
    );
  }
}

export default withStyles(styles)(connect(CustomInput));
