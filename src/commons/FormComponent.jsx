import React from "react";
import { setLocale } from "yup";
import PropTypes from "prop-types";
import { Form, withFormik } from "formik";
import { yupCustomMessages } from "../utils/yup-customs";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = (theme) => ({
  button: {
    marginTop: theme.spacing.unit * 3,
  },
});

const FormComponent = withFormik({
  mapPropsToValues: (props) => props.initialValues,
  validationSchema: (props) => props.validationSchema,
  handleSubmit: (values, { props, resetForm, setErrors, setSubmitting }) => {
    props.handleSubmit(values);
  },
});

const CustomForm = (props) => {
  const { classes, style } = props;
  const children = React.Children.map(props.children, (child) => {
    return React.cloneElement(child, {
      highlevelprops: props,
    });
  });

  setLocale(yupCustomMessages);

  return (
    <Form style={style}>
      <Grid container spacing={24}>
        {children}
      </Grid>
      <Grid container spacing={8} justify="center">
        {props.buttons
          .filter((it) => it.visible !== false)
          .map((button, key) => {
            let features = {};
            if (button.onClick) features = { onClick: button.onClick };
            return (
              <Grid item xs={12} key={key}>
                <Button {...button} className={classes.button} {...features}>
                  {button.label}
                </Button>
              </Grid>
            );
          })}
      </Grid>
    </Form>
  );
};

FormComponent.propTypes = {
  initialValues: PropTypes.object.isRequired,
  fields: PropTypes.array.isRequired,
  validationSchema: PropTypes.object,
  buttons: PropTypes.array.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleClose: PropTypes.func,
};

export default FormComponent(withStyles(styles)(CustomForm));
