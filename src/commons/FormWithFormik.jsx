import React, { Fragment } from "react";

import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import withStyles from "@material-ui/core/styles/withStyles";
import { withFormik, Form, Field, ErrorMessage } from "formik";
import CustomErrorMessageComponent from "./fields/CustomErrorMessageComponent";
import { errorMessageClass } from "./layout/theme";

const styles = (theme) => ({
  errorMessage: errorMessageClass,
  button: {
    marginTop: theme.spacing.unit * 3,
  },
});

const FormWithFormik = withFormik({
  mapPropsToValues: (props) => props.initialValues,
  validationSchema: (props) => props.validationSchema,
  handleSubmit: (values, { props, resetForm, setErrors, setSubmitting }) => {
    props.handleSubmit(values);
  },
});

const CustomInputComponent = ({ field, ...props }) => {
  const { options } = props;

  if (options) {
    return (
      <Select {...field} {...props}>
        <MenuItem value="">
          <em></em>
        </MenuItem>
        {options.map((it, i) => (
          <MenuItem key={i} value={it.value}>
            {it.label}
          </MenuItem>
        ))}
      </Select>
    );
  } else {
    return <TextField {...field} {...props} />;
  }
};

const CustomForm = (props) => {
  const { classes, formStyle } = props;
  return (
    <Form style={formStyle}>
      {props.fields.map((field, key) => {
        return (
          <Fragment key={key}>
            <FormControl margin="normal" fullWidth>
              <Field
                {...field}
                {...{
                  component: CustomInputComponent,
                  highlevelprops: props,
                  variant: field.variant,
                  margin: field.margin,
                }}
              />
              <ErrorMessage
                name={field.name}
                className={classes.errorMessage}
                component={CustomErrorMessageComponent}
              />
            </FormControl>
          </Fragment>
        );
      })}
      {props.buttons.map((button, key) => {
        let features = {};
        if (button.onClick) features = { onClick: button.onClick };
        return (
          <Button
            key={key}
            {...button}
            className={classes.button}
            {...features}
          >
            {button.label}
          </Button>
        );
      })}
    </Form>
  );
};

export default FormWithFormik(withStyles(styles)(CustomForm));
