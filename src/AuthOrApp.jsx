import React, { Component } from "react";
import { connect } from "react-redux";
import { compose, bindActionCreators } from "redux";
import { withRouter } from "react-router";
import { Route, Switch } from "react-router-dom";

import axios from "axios";
import resources from "./main/resources";
import App from "./App";
import Auth from "./Auth";
import SignIn from "./views/auth/SignIn";
import SignUp from "./views/auth/SignUp";

class AuthOrApp extends Component {
  componentWillReceiveProps(props) {
    const authenticated = props.auth.user ? true : false;
    const currentLocation = props.history.location.pathname;
    if (authenticated && currentLocation === `/${resources.SIGNIN_RESOURCE}`) {
      props.history.push(`/${resources.PROJECTS_RESOURCE}`);
    } else if (
      !authenticated &&
      currentLocation !== `/${resources.SIGNIN_RESOURCE}` &&
      currentLocation !== `/${resources.SIGNUP_RESOURCE}`
    ) {
      props.history.push(`/${resources.SIGNIN_RESOURCE}`);
    }
  }

  render() {
    const { auth, history } = this.props;
    if (!auth.user && history.location) {
      return <Auth>{this.renderView()}</Auth>;
    } else {
      axios.defaults.headers.common["Authorization"] = `${auth.user.token}`;
      return <App>{this.props.children}</App>;
    }
  }

  renderView() {
    return (
      <Switch>
        <Route path="/signup" component={SignUp} />
        <Route component={SignIn} />
      </Switch>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});
const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);
export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(AuthOrApp);
