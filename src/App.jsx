import React, { Component } from "react";
import { compose, bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { withSnackbar } from "notistack";
//import classNames from 'classnames';
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
//import MenuIcon from '@material-ui/icons/Menu';
import IconButton from "@material-ui/core/IconButton";
//import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import SettingsPower from "@material-ui/icons/SettingsPower";
import { addMessage, removeMessage } from "./state/app/appActions";
import { doLogout } from "./state/auth/authActions";

import "./App.css";

const drawerWidth = 240;

const styles = (theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
    // visibility: 'hidden'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    textDecoration: "none",
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
    fontSize: "1.75rem",
    fontWeight: 300,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: "100vh",
    overflow: "auto",
  },
  chartContainer: {
    marginLeft: -22,
  },
  tableContainer: {
    height: 320,
  },
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
  link: {
    color: "inherit",
  },
  avatarContainer: {
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing.unit * 9,
    },
    minWidth: 56,
    display: "flex",
    justifyContent: "center",
  },
  userInfo: {
    marginRight: 10,
  },
});

class App extends Component {
  state = {
    showProfileMenu: null,
    open: false,
  };

  logout = () => {
    confirmAlert({
      title: "Logout",
      message: "Are you sure?",
      buttons: [
        {
          label: "OK",
          onClick: () => {
            this.props.doLogout().then(() => {
              this.props.history.push("/signin");
            });
          },
        },
        {
          label: "Cancel",
        },
      ],
    });
  };

  goToRoute = (route) => {
    this.props.history.push(`/${route}`);
  };

  handleClickVariant = (message, variant) => () => {
    const { addMessage } = this.props;
    addMessage({ message, variant });
  };

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    if (this.props.messageDataList !== nextProps.messageDataList) {
      nextProps.messageDataList.forEach((messageData) => {
        const variant = messageData.variant;
        this.props.removeMessage(messageData);
        this.props.enqueueSnackbar(messageData.message, { variant });
      });
    }
  }

  render() {
    const { classes, auth } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="absolute">
          <Toolbar className={classes.toolbar}>
            <Typography color="inherit" noWrap className={classes.title}>
              EDirectinsure TODO List
            </Typography>
            <div className={classes.userInfo}>
              <Typography variant="subtitle2">{auth.user.username}</Typography>
            </div>
            <Tooltip title="Logout" aria-label="Logout" placement="bottom">
              <IconButton color="inherit" onClick={this.logout}>
                <SettingsPower />
              </IconButton>
            </Tooltip>
          </Toolbar>
        </AppBar>
        <main className={classes.content}>{this.props.children}</main>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  messageDataList: state.app.messageDataList,
  auth: state.auth,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      addMessage,
      removeMessage,
      doLogout,
    },
    dispatch
  );
export default compose(
  withRouter,
  withSnackbar,
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(App);
