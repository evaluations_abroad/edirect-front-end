import { combineReducers } from 'redux';

import AppReducer from "../state/app/appReducer";
import AuthReducer from "../state/auth/authReducer";
import ProjectReducer from '../views/project/ducks/reducer';

const appReducer = combineReducers({
  app: AppReducer,
  auth: AuthReducer,
  project: ProjectReducer
});

const rootReducer = (state, action) => {
  if (action.type === 'CLEAR_STORE') 
    return appReducer(undefined, action);
  return appReducer(state, action);
};

export default rootReducer;
