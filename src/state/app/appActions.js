export const types = {
  APP_ADD_MESSAGE: 'APP_ADD_MESSAGE',
  APP_REMOVE_MESSAGE: 'APP_REMOVE_MESSAGE',
  APP_SHOW_SNACKBAR: 'APP_SHOW_SNACKBAR',
};

export const addMessage = (data) => {
  return {
    type: types.APP_ADD_MESSAGE,
    payload: { ...data, timestamp: new Date().getTime() }
  }
}

export const removeMessage = (data) => {
  return {
    type: types.APP_REMOVE_MESSAGE,
    payload: data
  }
}

export const showErrorMessage = (error) => dispatch => {
  dispatch(showMessage(error, 'error'));
}

export const showErrorMessages = (errors = []) => dispatch => {
  errors.forEach(it => {
    dispatch(showMessage(JSON.stringify(it.message), 'error'));
  });
}

export const showMessage = (message, variant = 'success') => dispatch => {
  dispatch(addMessage({ message, variant }));
}