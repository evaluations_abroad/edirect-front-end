import { types } from './appActions';

const INITIAL_STATE = {
  messageDataList: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.APP_ADD_MESSAGE:
      return {
        ...state,
        messageDataList: [...state.messageDataList, action.payload]
      };
    case types.APP_REMOVE_MESSAGE:
      return {
        ...state,
        messageDataList: state.messageDataList.filter(
          item => item.timestamp !== action.payload.timestamp
        )
      };
    case types.APP_SHOW_SNACKBAR:
      return { ...state };
    default:
      return state;
  }
};