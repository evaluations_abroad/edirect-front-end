import { types } from "./authActions";
import consts from "../../main/consts";

const userKey = consts.USER_KEY;

const INITIAL_STATE = {
  loading: false,
  user: JSON.parse(localStorage.getItem(userKey)),
  users: { rows: [], total: 0 },
  myAccountFormOpened: false,
  changePasswordFormOpened: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.AUTH_BEGIN:
      return { ...state, loading: true };
    case types.AUTH_SIGNIN:
      localStorage.setItem(userKey, JSON.stringify(action.payload));
      return { ...state, user: action.payload, loading: false };
    case types.AUTH_SIGNUP:
      return { ...state };
    case types.AUTH_ACCOUNT_UPDATED:
      localStorage.setItem(userKey, JSON.stringify(action.payload));
      return { ...state, user: action.payload };
    case types.AUTH_MY_ACCOUNT_FORM:
      return { ...state, myAccountFormOpened: action.payload };
    case types.AUTH_CHANGE_PASSWORD_FORM:
      return { ...state, changePasswordFormOpened: action.payload };
    case types.AUTH_LOGOUT:
      localStorage.removeItem(userKey);
      return { ...state, user: null };
    case types.AUTH_USERS_FETCHED:
      return { ...state, users: action.payload };
    case types.AUTH_USERS_FOR_APPROVAL_FETCHED:
      return { ...state, usersToApproval: action.payload };
    case types.AUTH_CLEAR_STORE:
      return { ...state, user: null };
    default:
      return state;
  }
};
