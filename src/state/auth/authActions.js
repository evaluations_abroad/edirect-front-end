import axios from "axios";

import consts from "../../main/consts";
import { showErrorMessage } from "../app/appActions";
import ApiAgent, { HTTP_METHOD } from "../../main/apiAgent";
import resources from "../../main/resources";

export const types = {
  AUTH_BEGIN: "AUTH_BEGIN",
  AUTH_SIGNUP: "AUTH_SIGNUP",
  AUTH_SIGNIN: "AUTH_SIGNIN",
  AUTH_LOGOUT: "AUTH_LOGOUT",
  AUTH_CLEAR_STORE: "CLEAR_STORE",
};

export const doSignUp = (values) => (dispatch) => {
  // const variables = { signUpInput: { ...values } };
  dispatch({ type: types.AUTH_BEGIN });
  return ApiAgent.submit(resources.USERS_RESOURCE, HTTP_METHOD.POST, {
    params: values,
  });
};

export const doSignIn = async (values) => (dispatch, getState) => {
  dispatch({ type: types.AUTH_BEGIN });
  axios
    .post(`${consts.API_URL}/${resources.USERS_RESOURCE}/login`, values)
    .then((resp1) => {
      const user = resp1.data;
      axios.defaults.headers.common["Authorization"] = `${user.id}`;
      axios
        .get(`${consts.API_URL}/${resources.USERS_RESOURCE}/${user.userId}`, {
          params: {},
        })
        .then((resp2) => {
          user.token = user.id;
          user.id = user.userId;
          user.username = resp2.data.email;
          delete user.userId;
          dispatch([
            {
              type: types.AUTH_SIGNIN,
              payload: { ...user },
            },
          ]);
          return user;
        });
    })
    .catch((e) => {
      if (e.message !== "Network Error") {
        dispatch(showErrorMessage("Wrong username or password!"));
      }
    });
};

export const doLogout = async () => (dispatch) => {
  dispatch({
    type: types.AUTH_LOGOUT,
  });
  dispatch({
    type: types.AUTH_CLEAR_STORE,
  });
};
