export default {
  API_URL: process.env.REACT_APP_API_URL,
  USER_KEY: process.env.REACT_APP_USER_KEY
};
