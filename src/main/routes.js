import React, { Fragment } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import { SnackbarProvider } from "notistack";
import { MuiThemeProvider } from "@material-ui/core/styles";

import AuthOrApp from "../AuthOrApp";
import theme from "../commons/layout/theme";
import SignIn from "../views/auth/SignIn";
import resources from "./resources";
import ProjectList from "../views/project/containers/ProjectList";

export default (props) => (
  <MuiThemeProvider theme={theme}>
    <BrowserRouter>
      <Switch>
        <>
          <SnackbarProvider maxSnack={3}>
            <Fragment>
              <AuthOrApp>
                <Route exact path="/" component={ProjectList} />
                <Route
                  path={`/${resources.SIGNIN_RESOURCE}`}
                  component={SignIn}
                />
                <Route
                  path={`/${resources.PROJECTS_RESOURCE}`}
                  component={ProjectList}
                />
              </AuthOrApp>
            </Fragment>
          </SnackbarProvider>
        </>
      </Switch>
    </BrowserRouter>
  </MuiThemeProvider>
);
