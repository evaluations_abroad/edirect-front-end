import consts from "./consts";
import axios from "axios";

import { gerarJsonFiltros } from "../utils/util";

export const HTTP_METHOD = {
  GET: "get",
  POST: "post",
  PUT: "put",
  DELETE: "delete",
};

const ApiAgent = {
  submit(resource, method, options) {
    const { params, config = {} } = options;
    const path = options.path ? `/${options.path}` : "";
    const id = params.id ? params.id : "";

    if (!params.id) {
      delete params.id;
    }

    if (method === HTTP_METHOD.GET) {
      config.params = params;
      delete params.id;
      return axios[method](
        `${consts.API_URL}/${resource}/${id}${path}`,
        config
      );
    } else {
      return axios[method](
        `${consts.API_URL}/${resource}/${id}${path}`,
        params,
        config
      );
    }
  },

  getList(resource, filters, where, type) {
    return (dispatch) => {
      return axios
        .get(`${consts.API_URL}/${resource}`, {
          params: {
            filter: gerarJsonFiltros(0, 0, where, filters),
          },
        })
        .then((response) => {
          dispatch({
            type: type,
            payload: response.data,
          });
        });
    };
  },
};

export default ApiAgent;
