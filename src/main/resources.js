export default {
  SIGNIN_RESOURCE: "signin",
  SIGNUP_RESOURCE: "signup",
  USERS_RESOURCE: "Users",
  PROJECTS_RESOURCE: "Projects",
  TASKS_RESOURCE: "Tasks",
};
