import React, { Component } from 'react';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { withSnackbar } from 'notistack';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';

import { removeMessage } from './state/app/appActions';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  logo: {
    margin: 10,
    width: 80,
    height: 80
  }
});

class Auth extends Component {
  componentWillReceiveProps(nextProps) {
    if (this.props.messageDataList !== nextProps.messageDataList) {
      nextProps.messageDataList.forEach(messageData => {
        const variant = messageData.variant;
        this.props.removeMessage(messageData);
        this.props.enqueueSnackbar(messageData.message, { variant });
      });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          {this.props.children}
        </Paper>
      </main>
    );
  }
}

const mapStateToProps = state => ({
  messageDataList: state.app.messageDataList
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ removeMessage }, dispatch);
export default compose(
  withRouter,
  withSnackbar,
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Auth);
